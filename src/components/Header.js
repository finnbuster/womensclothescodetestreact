import React, { Component } from 'react';
import { connect } from 'react-redux';
import { updateItemsCategory } from '../actions/items';

class Header extends Component {
  constructor(props) {
    super(props);

    this.changeFilter = this.changeFilter.bind(this);
  }

  changeFilter(event) {
    this.props.changeCategory(event.target.value);
  }

  render() {
    // sizes starts as an empty array
    let sizes = []
    // map through all products and push each siae from each product array of sizes to the sizes variable
    this.props.items.map(function(product) {
      product.size.map(function(size) {
        sizes.push(size)
      })
    })
    // filter unique sizes
    sizes = [...new Set(sizes)]
    // create an empty string variable for size elements
    let sizesElements = ''
    // if there are any sizes, map over them and output the appropriate option elements
    if(sizes.length > 0) {
      sizesElements = sizes.map((item) => (
        <option key={item} value={item}>{item}</option>
      ))
    }    

    return(
      <header className="bg-blue-100 text-grey-400 flex justify-between mt-10 mb-3 p-3">
        <h1 className="text-sm md:text-2xl">Womens Tops</h1>
        <select onChange={this.changeFilter}>
          <option value="">Filter by size</option>
          {sizesElements}
        </select>
      </header>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    items: state.items,
    itemsCategory: state.itemsCategory
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    changeCategory: (category) => dispatch(updateItemsCategory(category))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Header)