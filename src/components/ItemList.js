import React, { Component } from 'react';
import { connect } from 'react-redux';
import { itemsFetchData } from '../actions/items';

import Header from './Header'
import ProductCard from './ProductCard'

class ItemList extends Component {
  componentDidMount() {
    // passing api endpoint to fetchData dispatch in store
    this.props.fetchData('https://api.jsonbin.io/b/5cae9a54fb42337645ebcad3');
  }

  render() {
    // show error if there was an issue loading products
    if (this.props.hasErrored) {
      return (
        <section className="container">
          <Header />
          <div style={productsStyle} className="flex flex-wrap">
            <p>Sorry! There was an error loading the items</p>
          </div>
        </section>
      )
    }
    // show loading message while loading
    else if (this.props.isLoading) {
      return (
        <section className="container">
          <Header />
          <div style={productsStyle} className="flex flex-wrap">
            <p>Loading…</p>
          </div>
        </section>
      )
    }
    else {
      let products = ''
      // check if a category has been set in vuex. If it has, filter the products in vuez by the currently selected size category
      if(this.props.itemsCategory !== '') {
        products = this.props.items.filter((item) => {
          return item.size.find((size) => size === this.props.itemsCategory)
        }).map((item) => (
          <ProductCard product={item} key={item.index} />
        ))
      // otherwise show all products
      } else {
        products = this.props.items.map((item) => (
          <ProductCard product={item} key={item.index} />
        ))
      }
      return (
        <section className="container">
          <Header />
          <div style={productsStyle} className="flex flex-wrap">
            {products}
          </div>
        </section>
      )
    }
  }
}

const productsStyle = {
  margin: `1px`
}

const mapStateToProps = (state) => {
  return {
    items: state.items,
    itemsCategory: state.itemsCategory,
    hasErrored: state.itemsHasErrored,
    isLoading: state.itemsIsLoading
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchData: (url) => dispatch(itemsFetchData(url))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ItemList);