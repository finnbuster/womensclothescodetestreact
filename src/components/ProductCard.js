import React, { Component } from 'react';

class ProductCard extends Component {
  render() {

    const product = this.props.product
    let productExtraInfo = <div className="text-white text-xs px-5 py-2 inline-block">&nbsp;</div>
    if(product.isSale)  {
      productExtraInfo = <div className="text-white text-xs px-5 py-2 inline-block bg-red-600">Sale</div>
    } else if(product.isExclusive) {
      productExtraInfo = <div className="text-white text-xs px-5 py-2 inline-block bg-green-600">Exclusive</div>
    }

    const imgUrl = `/img/${product.productImage}`

    return (
      <div style={productStyle} className="product w-1/2 md:w-1/4 border p-2 pb-5 flex flex-col">
        <div>
          <img src={imgUrl} alt={product.productName} width="280" height="280" className="block" />
          { productExtraInfo }
        </div>
        <div className="mt-5 flex justify-between items-end flex-grow text-gray-700">
          <h3 className="font-bold md:text-md mr-2">{ product.productName }</h3>
          <div className="font-bold text-lg md:text-2xl md:leading-tight">{ product.price }</div>
        </div>
      </div>
    )
  }
}

const productStyle = {
  margin: '0px',
  boxShadow: `0 0 1px lightgrey`
}

export default ProductCard