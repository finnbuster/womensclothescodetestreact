import { combineReducers } from 'redux';
import { items, itemsCategory, itemsHasErrored, itemsIsLoading } from './items';

export default combineReducers({
  items,
  itemsCategory,
  itemsHasErrored,
  itemsIsLoading
});